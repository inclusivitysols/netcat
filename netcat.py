#!/usr/bin/python
"""
Simple script to check whether or not a host is able to connect to one or multiple other hosts.

Command reference:

netcat.py [parameters] <ip>

Parameters:

ip                 IP address of endpoint to be checked
-p, --port         Port of endpoint to be checked. (Defaults to 80)
-gw, --gateway     IP Address of the VPN gateway to be checked
-d, --description  Alarm description

Example usage: python netcat.py -d "Example connection" -p 8310 -gw 10.99.132.110 10.206.70.213

"""#
import argparse
from time import sleep

import argparse_actions
import socket
import logging
import os
import subprocess
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from smtpd import COMMASPACE
from email.utils import formatdate
import smtplib

seconds_count = 0
initial_connection = 1


class EnvDefault(argparse.Action):
    def __init__(self, envvar, required=True, default=None, **kwargs):
        if not default and envvar:
            if envvar in os.environ:
                default = os.environ[envvar]
        if required and default:
            required = False
        super(EnvDefault, self).__init__(default=default, required=required,
                                         **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, self.dest, values)

class IpAndHostAction(argparse_actions.ProperIpFormatAction):
    def __call__(self, parser, namespace, values, option_string=None):
        values = socket.gethostbyname(values)
        parent = super(IpAndHostAction, self)
        parent.__call__(parser, namespace, values, option_string)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='This is a connectivity checker that logs exceptions to Sumo Logic.')
    parser.add_argument('ip', help='IP address of endpoint to be checked', action=IpAndHostAction)
    parser.add_argument('-p','--port', help='Port of endpoint to be checked. (Defaults to 80)', required=False, default=80, type=int)
    parser.add_argument('-gw','--gateway', help='IP Address of the VPN gateway to be checked', required=False, action=argparse_actions.ProperIpFormatAction)
    parser.add_argument(
        "-d", "--description", dest="description",
        help="Alarm description", required=False)
    parser.add_argument('-t', '--time',
                        help="This is the time to sleep before making another call",
                        required=False, default=3, type=int)

    args=parser.parse_args()

    if args.description == None:
        description = ""
    else:
        description = " name=" + args.description

logging.basicConfig(level=logging.DEBUG)

def ping(host):
    result = subprocess.call(["ping","-c","1",host],stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    if result == 0:
        logging.warning("CANARY: Gateway to=%s%s status=1" % (args.gateway, description))
        return True
    elif result == 1:
        logging.error("CANARY: Gateway to=%s%s status=0 exception='Host not found'" % (args.gateway, description))
    elif result == 2:
        logging.error("CANARY: Gateway to=%s%s status=0 exception='Ping timed out'" % (args.gateway, description))


def send_mail(subject, text, silent_mode=True):
    send_to = str(os.environ.get("mail_recipients")).split()
    send_from = os.environ.get("from_mail")
    assert isinstance(send_to, list)
    msg = MIMEMultipart()
    msg['From'] = os.environ.get("from_mail")
    if silent_mode:
        msg['To'] = COMMASPACE.join('elias@inclusivitysolutions.com')
    else:
        msg['To'] = COMMASPACE.join(send_to)
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = subject
    msg.attach(MIMEText(text))
    server = smtplib.SMTP(os.environ.get("smtp_host_address"), 25)
    server.starttls()
    server.login(os.environ.get("mail_username"), os.environ.get("mail_password"))
    server.sendmail(send_from, send_to, msg.as_string())
    server.close()

def restart_tunnel(silent_mode=False):
    orange_stack_uuid = os.environ.get('orange_stack_uuid')
    import requests
    url = "https://cloud.docker.com/api/app/v1/inclusivity/stack/{orange_stack_uuid}/redeploy/".format(orange_stack_uuid=orange_stack_uuid)
    credentials = os.environ.get("credentials")
    headers = {
        'authorization': "Basic {}".format(credentials),
        'content-type': "application/json",
        'cache-control': "no-cache",
    }
    response = requests.request("POST", url, headers=headers)
    subject = "ORANGE PING  Redeploy"
    body = "ORANGE PING RESTARTED \n\n {} ".format(response.text)
    send_mail(subject, body, silent_mode=silent_mode)

while True:
    s = socket.socket()
    s.settimeout(10)
    try:
        s.connect((args.ip, args.port))
    except Exception as e:
        alerttext = "CANARY: Endpoint to=%s:%s%s status=0 exception='%s'" % (args.ip, args.port, description, str(e))

    if not args.gateway == None:
        try:
            result = ping(args.gateway)
            if result:
                initial_connection = 0
        except Exception as e:
            if initial_connection == 0:
                restart_tunnel()
            if args.description == None:
                description = " name=VPN"
            alerttext = "CANARY: Endpoint to=%s%s status=0 exception='%s'" % (args.gateway, description, e)


    if 'alerttext' in locals():
        print(alerttext)
        logging.error(alerttext)
    else:
        infotext = "CANARY: Endpoint to=%s:%s%s status=1" % (args.ip, args.port, description)
        print(infotext)
        logging.warning(infotext)
    s.close()
    logging.info('sleep {}'.format(args.time))
    time_to_add = args.time
    seconds_count += int(args.time)
    logging.info('time up in seconds {}'.format(str(seconds_count)))
    sleep(args.time)
